Python_App
=========

This role builds and runs a python executable from source.

Role Variables
--------------

- python_app_repo: The repo to pull from
- python_app_name: The app name
- python_app_config_dir: The config directory
- python_app_config_file: The config file
- python_app_dir: The directory to pull the app to [Default: /var/www]
- python_app_user: The user to own config directory
- python_app_group: The group to own the config directory
- python_app_binary: The app binary to run
- python_app_logfile: The logfile for the app
- python_app_source: The specific name of the python as built
- python_app_version: The app version
- python_app_config: Dict of yaml code to put into the config file
- python_web_server: Enables mod_cache if set to "apache" [Default: Apache]

Dependencies
------------

- geerlingguy.git
- Stouts.python

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: python_app }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
